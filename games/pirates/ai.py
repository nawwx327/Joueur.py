from joueur.base_ai import BaseAI
import math
import time
import random
# <<-- Creer-Merge: imports -->> - Code you add between this comment and the end comment will be preserved between Creer re-runs.
# you can add additional import(s) here
# <<-- /Creer-Merge: imports -->>

class CustomGame:
    def __init__(self):
        self.tiles_for_burial = []
        self.tiles_with_my_gold = []
        self.my_ships = []
        self.my_crews = []
        self.gold_guard_ships = None
        self.built_last = "ship"

MyGame = CustomGame()

class AI(BaseAI):
    """ The basic AI functions that are the same between games. """

    def get_name(self):
        """ This is the name you send to the server so your AI will control the player named this string.

        Returns
            str: The name of your Player.
        """
        # <<-- Creer-Merge: get-name -->> - Code you add between this comment and the end comment will be preserved between Creer re-runs.
        return "Pirates Python Player" # REPLACE THIS WITH YOUR TEAM NAME
        # <<-- /Creer-Merge: get-name -->>

    def start(self):
        """ This is called once the game starts and your AI knows its playerID and game. You can initialize your AI here.
        """
        # <<-- Creer-Merge: start -->> - Code you add between this comment and the end comment will be preserved between Creer re-runs.
        # replace with your start logic
        # <<-- /Creer-Merge: start -->>

    def game_updated(self):
        """ This is called every time the game's state updates, so if you are tracking anything you can update it here.
        """
        # <<-- Creer-Merge: game-updated -->> - Code you add between this comment and the end comment will be preserved between Creer re-runs.
        # replace with your game updated logic
        # <<-- /Creer-Merge: game-updated -->>

    def end(self, won, reason):
        """ This is called when the game ends, you can clean up your data and dump files here if need be.

        Args:
            won (bool): True means you won, False means you lost.
            reason (str): The human readable string explaining why you won or lost.
        """
        # <<-- Creer-Merge: end -->> - Code you add between this comment and the end comment will be preserved between Creer re-runs.
        # replace with your end logic
        # <<-- /Creer-Merge: end -->>

    def run_turn(self):
        """ This is called every time it is this AI.player's turn.

        Returns:
            bool: Represents if you want to end your turn. True means end your turn, False means to keep your turn going and re-call this function.
        """
        # <<-- Creer-Merge: runTurn -->> - Code you add between this comment and the end comment will be preserved between Creer re-runs.
        # Put your game logic here for runTurn

        ship_count = 0
        crew_count = 0
        for unit in self.player.units:
            print('Turn', self.game.current_turn)
            print(unit, ' ---- Current crew', unit.crew)
            print(unit, ' ---- Current ship', unit.ship_health)
            print(unit, ' ---- Current gold', unit.gold)
            if unit.crew != 0:
                crew_count += unit.crew
            if unit.ship_health != 0:
                ship_count += 1

        if crew_count <= ship_count and self.player.port.tile.unit == None and ship_count <= 5:
            self.player.port.spawn("crew")
            crew_count += 1
        # Spawn a ship so our crew can sail if there are more crew and there is not a ship in the port
        elif crew_count > ship_count and ship_count <= 5:
            self.player.port.spawn("ship")
            ship_count += 1

        # all i want to do is make the ships attack each other
        if ship_count > 0 and ship_count < 5:

            for unit in self.player.units:
                # find the closest port and merchant
                best_port = self.closest_merchant_port(unit)
                best_merch = self.closest_merchant_ship(unit)

                # if on the port and not fully healed, heal
                if unit.tile == self.player.port.tile and unit.ship_health != 20:
                    unit.rest()
                # if a ship has more than 1200 gold, return
                elif unit.gold >= 1200:
                    path = self.find_path(unit.tile, unit.owner.port.tile, unit)
                    # if the ship is on the home port
                    if len(path) == 1:
                        # deposit it's money and rest
                        unit.deposit()
                    else:
                        # otherwise get closer
                        unit.move(path[0])
                # if the ship is really hurt
                elif unit.ship_health < 10:
                    path = self.find_path(unit.tile, unit.owner.port.tile, unit)
                    # if the ship is on the home port
                    if len(path) == 0:
                        # deposit it's money and rest
                        unit.deposit()
                        unit.rest()
                    else:
                        # otherwise get closer
                        unit.move(path[0])
                # if there are merchants...
                elif best_merch is not None:
                    # if there is a merchant in the range of the ship, shoot it
                    if self.get_distance_to(unit.tile, best_merch.tile) == 1:
                        if random.randint(0, 1) == 0 and unit.crew > 1:
                            unit.split(best_merch.tile)
                    if self.get_distance_to(unit.tile, best_merch.tile) <= self.game.ship_range:
                        unit.attack(best_merch.tile, "ship")
                # if there are no merchants
                else:
                    # not enough gold to go back
                    # move closer to the merchant port
                    path = self.find_path(unit.tile, best_port.tile, unit)
                    if len(path) > 1:
                        unit.move(path[0])
        # when there are enough ships, attack
        else:
            for unit in self.player.units:
                close_enemy = self.closest_enemy_ship(unit)

                # if the enemy is in range, fire
                if self.get_distance_to(unit.tile, close_enemy.tile) <= self.game.ship_range:
                    unit.attack(close_enemy.tile, "ship")
                # else move closer
                else:
                    path = self.find_path(unit.tile, close_enemy.tile, unit)
                    try:
                        if len(path) != 1:
                            unit.move(path[0])
                    except IndexError:
                        continue


        return True
        # <<-- /Creer-Merge: runTurn -->>

    def find_path(self, start, goal, unit):
        """A very basic path finding algorithm (Breadth First Search) that when given a starting Tile, will return a valid path to the goal Tile.
        Args:
            start (Tile): the starting Tile
            goal (Tile): the goal Tile
            unit (Unit): the Unit that will move
        Returns:
            list[Tile]: A list of Tiles representing the path, the the first element being a valid adjacent Tile to the start, and the last element being the goal.
        """

        if start == goal:
            # no need to make a path to here...
            return []

        # queue of the tiles that will have their neighbors searched for 'goal'
        fringe = []

        # How we got to each tile that went into the fringe.
        came_from = {}

        # Enqueue start as the first tile to have its neighbors searched.
        fringe.append(start)

        # keep exploring neighbors of neighbors... until there are no more.
        while len(fringe) > 0:
            # the tile we are currently exploring.
            inspect = fringe.pop(0)

            # cycle through the tile's neighbors.
            for neighbor in inspect.get_neighbors():
                # if we found the goal, we have the path!
                if neighbor == goal:
                    # Follow the path backward to the start from the goal and return it.
                    path = [goal]

                    # Starting at the tile we are currently at, insert them retracing our steps till we get to the starting tile
                    while inspect != start:
                        path.insert(0, inspect)
                        inspect = came_from[inspect.id]
                    return path
                # else we did not find the goal, so enqueue this tile's neighbors to be inspected

                # if the tile exists, has not been explored or added to the fringe yet, and it is pathable
                if neighbor and neighbor.id not in came_from and neighbor.is_pathable(unit):
                    # add it to the tiles to be explored and add where it came from for path reconstruction.
                    fringe.append(neighbor)
                    came_from[neighbor.id] = inspect

        # if you're here, that means that there was not a path to get to where you want to go.
        #   in that case, we'll just return an empty path.
        return []

    # <<-- Creer-Merge: functions -->> - Code you add between this comment and the end comment w
    # ill be preserved between Creer re-runs.
    # if you need additional functions for your AI you can add them here
    # <<-- /Creer-Merge: functions -->>


    # returns a list of merchant ships
    def find_merchant_ships(self):
        merchants = []
        for u in self.game.units:
            if u.target_port is not None and u.ship_health > 0:
                merchants.append(u)
        return merchants

    def find_enemy_ships(self, unit):
        enemies = []
        for enemy_unit in self.game.units:
            if enemy_unit.owner is not unit.owner and enemy_unit.owner is not None:
                enemies.append(enemy_unit)
        return enemies

    def get_distance_to(self, from_tile, to_tile):
        try:
            distanceX = from_tile.x - to_tile.x
            distanceY = from_tile.y - to_tile.y
        except AttributeError:
            return 500
        return math.sqrt(distanceX * distanceX + distanceY * distanceY);

    def is_ship(self, unit):
        if unit.ship_health > 0:
            return True
        return False

    def is_crew(self, unit):
        if unit.crew > 0:
            return True
        return False

    # returns the closest port to my location
    def closest_merchant_port(self, ship):
        merch_ports = []
        for port in self.game.ports:
            if port.owner == None:
                merch_ports.append(port)

        best_dist = 800
        best_port = None
        for port in merch_ports:

            try:
                dist = self.get_distance_to(port.tile, ship.tile)
            except AttributeError:
                continue

            if dist < best_dist:
                best_dist = dist
                best_port = port

        return best_port

    def closest_merchant_ship(self, ship):
        merch_ships = self.find_merchant_ships()
        best_dist = 800
        best_ship = None
        for m_ship in merch_ships:
            # print(type(m_ship))
            try:
                dist = self.get_distance_to(m_ship.tile, ship.tile)
            except AttributeError:
                continue
            # print('best, new:  {}  {}'.format(best_dist, dist))
            if dist < best_dist:
                best_dist = dist
                best_ship = m_ship
        # print(best_ship)
        # print('return: ' , best_dist)
        return best_ship

    def closest_enemy_ship(self, ship):
        enemy_ships = self.find_enemy_ships(ship)
        # print('numMerchShips: ', len(merch_ships))
        best_dist = 800
        best_ship = None
        for m_ship in enemy_ships:
            # print(type(m_ship))
            try:
                dist = self.get_distance_to(m_ship.tile, ship.tile)
            except AttributeError:
                continue
            # print('best, new:  {}  {}'.format(best_dist, dist))
            if dist < best_dist:
                best_dist = dist
                best_ship = m_ship
        # print(best_ship)
        # print('return: ' , best_dist)
        return best_ship

    def check_attack(self, unit, flag):
        if flag == "all":
            # opponent ships and their ships (all the ships that aren't mine)
            for any_unit in self.game.units:
                if any_unit.owner is not unit.owner:
                    any_path = self.find_path(unit.tile, any_unit.tile, unit)
                    if self.get_distance_to(any_unit.tile, unit.tile) <= self.game.ship_range:
                        unit.attack(any_unit.tile, "ship")
        if flag == "merchant":
            # ships that are owned by no one
            for merchant_unit in self.game.units:
                if merchant_unit.owner is None:
                    merchant_path = self.find_path(unit.tile, merchant_unit.tile, unit)
                    if self.get_distance_to(merchant_unit.tile, unit.tile) <= self.game.ship_range:
                        unit.attack(merchant_unit.tile, "ship")
        if flag == "enemy":
            # ships not owned by you and no one
            for enemy_unit in self.game.units:
                if enemy_unit.owner is not unit.owner and enemy_unit.owner is not None:
                    print('enemy: ', enemy_unit)
                    print('me   : ', unit)
                    print('this is an enemy!!')
                    enemy_path = self.find_path(unit.tile, enemy_unit.tile, unit)
                    if len(enemy_path) > 1:
                        enemy_unit.move(enemy_path[0])
                    if self.get_distance_to(enemy_unit.tile, unit.tile) <= self.game.ship_range:
                        unit.attack(enemy_unit.tile, "ship")

    def check_tiles(self):
        # check to see if three adjacent tiles to the port are taken
        tile_count = 0

        if self.closest_merchant_port(self.player.units[0]).tile.tile_north.unit != None:
            tile_count += 1
        if self.closest_merchant_port(self.player.units[0]).tile.tile_south.unit != None:
            tile_count += 1
        if self.closest_merchant_port(self.player.units[0]).tile.tile_west.unit != None:
            tile_count += 1
        if self.closest_merchant_port(self.player.units[0]).tile.tile_east.unit != None:
            tile_count += 1
        if tile_count >= 3:
            # checks if any player ships can attack a ship in range
            for attempt_attack in self.player.units:
                if attempt_attack.ship_health > 0:
                    self.check_attack(attempt_attack, "all")











# end
